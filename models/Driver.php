<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "driver".
 *
 * @property int $id
 * @property string $name
 * @property string $passport
 * @property string $phone
 * @property string $truck_number
 *
 * @property Order[] $orders
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'passport', 'phone', 'truck_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'passport' => 'Паспорт/Водит. удост.',
            'phone' => 'Телефон',
            'truck_number' => 'Марка и гос.№ трансп. средства',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['driver_id' => 'id']);
    }
}

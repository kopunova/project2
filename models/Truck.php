<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "truck".
 *
 * @property int $id
 * @property string $name
 * @property double $tonnage
 */
class Truck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'truck';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['name'], 'required'],
			[['name'], 'string', 'max' => 255],
            [['tonnage'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'tonnage' => 'Тоннаж, т',
        ];
    }
}

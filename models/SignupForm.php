<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class SignupForm extends Model {

    public $email;
    public $username;
    public $phone;
    public $password;

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['email', 'username', 'password'], 'required'],
                [['email', 'username', 'password'], 'string', 'max' => 50],
                ['username', 'string', 'min' => 2],
                ['password', 'string', 'min' => 3],
                [['email', 'username', 'password'], 'trim'],
                ['email', 'email'],
                ['email', 'unique', 'targetClass' => 'app\models\User', 'message' => 'Этот Email уже занят.'],
        ];
    }

    public function attributeLabels() {
        return [
            'email' => 'Email',
            'username' => 'ФИО',
            'password' => 'Пароль',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->email = $this->email;
        $user->username = $this->username;
        $user->password = $this->password;
        $user->registration_date = date("Y-m-d");

        return $user->save() ? $user : null;
    }

}

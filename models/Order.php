<?php

namespace app\models;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $date_add
 * @property string $status
 * @property double $sum
 * @property double $weight
 * @property string $receiver_date
 * @property int $receiver_city
 * @property string $receiver_address
 * @property string $receiver_name
 * @property string $receiver_phone
 * @property string $receiver_email
 * @property int $sender_city
 * @property string $sender_address
 * @property string $comment
 *
 * @property City $receiverCity
 * @property City $senderCity
 */
class Order extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'order';
    }

    /* Возвращает список заявок "пригодных" для рейса */
	public static function getOrdersForShipping($shipping) {

        // 1. Нам необходимы "свободные" заявки, т.е. еще не в рейсах.
        $orders = Order::find()->join('LEFT JOIN', 'shipping_detail', 'order.id = order_id')
            ->where('order_id IS NULL')->orderBy('order.id')->all();

        // 2. Город отправления и Город получения по заявке должен соответствовать маршруту рейса.
        $cities = $shipping->route->citiesObj;
        $ordersForShipping = [];
        foreach ($orders as $order) {
            $key_start = array_search($order->senderCity, $cities);
            $key_finish = array_search($order->receiverCity, $cities);

            if ($key_start === false || $key_finish === false || $key_finish <= $key_start) {
                continue;
            }
            $ordersForShipping[] = $order;
        }

		return $ordersForShipping;
    }	

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                [['sender_city', 'sender_address', 'receiver_city', 'receiver_address', 'receiver_date', 'weight'], 'required'],
                [['date_add', 'unload_type', 'payment_method', 'sender_time_from', 'sender_time_to', 'receiver_time_from', 'receiver_time_to'], 'safe'],
                [['sum', 'weight', 'cargo_volume'], 'number'],
                [['receiver_city', 'sender_city', 'driver_id'], 'integer'],
                [['status', 'cargo_quantity_seats', 'receiver_address', 'receiver_name', 'receiver_phone', 'receiver_email', 'sender_address', 'comment', 'client', 'performer'], 'string', 'max' => 255],
                [['receiver_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['receiver_city' => 'id']],
                [['sender_city'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['sender_city' => 'id']],
                [['sender_date', 'receiver_date'], 'filter', 'filter' => function($value) {
                    if ($value == '')
                        return NULL;
                    return date('Y-m-d', strtotime($value));
                }],
                [['cargo_name', 'payment_condition', 'sender', 'sender_name', 'sender_phone', 'receiver', 'driver', 'driver_phone', 'driver_passport', 'truck_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'Номер',
            'date_add' => 'Дата создания',
            'dateAddFormat' => 'Дата создания',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'sum' => 'Сумма',
            'weight' => 'Вес, т',
            'receiver_date' => 'Дата получения',
            'receiverDateFormat' => 'Дата получения',
            'receiver_city' => 'Город получения',
            'receiver_address' => 'Адрес получения',
            'receiver_name' => 'Имя представителя',
            'receiver_phone' => 'Телефон',
            'receiver_email' => 'Email',
            'sender_city' => 'Город отправления',
            'sender_address' => 'Адрес отправления',
            'senderCityName' => 'Город отправления',
            'receiverCityName' => 'Город получения',
            'comment' => 'Комментарий',
            'unload_type' => 'Вид загрузки/выгрузки',
            'unloadTypeName' => 'Вид загрузки/выгрузки',
            'cargo_name' => 'Наименование груза',
            'cargo_volume' => 'Объём груза, м3',
            'cargo_quantity_seats' => 'Кол-во мест, палет',
            'payment_method' => 'Способ оплаты',
            'paymentMethodName' => 'Способ оплаты',
            'payment_condition' => 'Условия оплаты',
            'sender' => 'Грузоотправитель',
            'sender_name' => 'Имя представителя',
            'sender_phone' => 'Телефон',
            'receiver' => 'Грузополучатель',
            'driverName' => 'Водитель',
            'driver_phone' => 'Телефон водителя',
            'driver_passport' => 'Паспорт/Водительское удостоверение',
            'truck_number' => 'Марка и гос. номер транспортного средства',
            'client' => 'Заказчик',
            'performer' => 'Исполнитель',
            'sender_date' => 'Дата отправления',
            'senderDateFormat' => 'Дата отправления',
            'sender_time_from' => 'Время отправления с',
            'sender_time_to' => 'Время отправления по',
            'receiver_time_from' => 'Время получения с',
            'receiver_time_to' => 'Время получения по',
            'driver_id' => 'Водитель',
        ];
    }

    public function getStatuses() {

        return [
            '1' => 'Новая',
            '2' => 'В работе',
            '3' => 'Выполнено',
        ];
    }

    public function getStatusName() {

        return $this->statuses[$this->status];
    }

    public function getUnloadTypes() {

        return [
            'tent' => 'Тент',
            'zadnaya' => 'Задняя',
        ];
    }

    public function getUnloadTypeName() {

        return $this->unloadTypes[$this->unload_type];
    }

    public function getPaymentMethods() {

        return [
            'cash' => 'Нал',
            'noncash' => 'Безнал',
            'noncashWithNDS' => 'Безнал с НДС',
        ];
    }

    public function getPaymentMethodName() {

        return $this->paymentMethods[$this->payment_method];
    }

    public function getDateAddFormat() {

       if ($this->date_add) {
			return date('d.m.Y H:i:s', strtotime($this->date_add));
		} else return '';
    }

    public function getReceiverDateFormat() {

        return date('d.m.Y', strtotime($this->receiver_date));
    }

    public function getSenderDateFormat() {

        return date('d.m.Y', strtotime($this->sender_date));
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverCity() {
        return $this->hasOne(City::className(), ['id' => 'receiver_city']);
    }

    public function getReceiverCityName() {
        if (isset($this->receiverCity))
            return $this->receiverCity->name;
        else
            return '';
    }

    public function getDriverInst() {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    public function getDriverName() {
        if (isset($this->driverInst))
            return $this->driverInst->name;
        else
            return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderCity() {
        return $this->hasOne(City::className(), ['id' => 'sender_city']);
    }

    public function getSenderCityName() {
        if (isset($this->senderCity))
            return $this->senderCity->name;
        else
            return '';
    }

    public function beforeSave($insert) {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->date_add = date('Y-m-d H:i:s');
        }

        return true;
    }

}

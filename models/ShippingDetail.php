<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shipping_detail".
 *
 * @property int $id
 * @property int $truck_id
 * @property int $order_id
 *
 * @property Order $order
 * @property ShippingTruck $truck
 */
class ShippingDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['truck_id', 'order_id'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['truck_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShippingTruck::className(), 'targetAttribute' => ['truck_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'truck_id' => 'Truck ID',
            'order_id' => 'Order ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(ShippingTruck::className(), ['id' => 'truck_id']);
    }
}

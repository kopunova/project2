<?php

namespace app\models;

use yii\web\IdentityInterface;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $email
 * @property string $name
 * @property string $password
 * @property string $registration_date
 * @property double $balance
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    public $role;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['email', 'password', 'registration_date', 'role'], 'required'],
                [['registration_date'], 'safe'],
                [['registration_date'], 'filter', 'filter' => function($value) {
                    if ($value == '')
                        return NULL;
                    return date('Y-m-d', strtotime($value));
                }],
                [['email', 'username', 'password'], 'string', 'max' => 255],
                [['email'], 'unique'],
                ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'username' => 'ФИО',
            'password' => 'Пароль',
            'registration_date' => 'Дата регистрации',
            'role' => 'Роль',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        /* foreach (self::$users as $user) {
          if ($user['accessToken'] === $token) {
          return new static($user);
          }
          } */

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        //return $this->authKey;
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        //return $this->authKey === $authKey;
        return null;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === md5($password);
    }

    public function setPassword($password) {
        //$this->password = $password;
		$this->password = md5($password);
    }
	
	public function beforeSave($insert) {

        if (!parent::beforeSave($insert)) {
            return false;
        }
				
		if (isset($this->dirtyAttributes['password'])) {
			$this::setPassword($this->dirtyAttributes['password']);
		}         

        return true;
    }

    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
        $role = $auth->getRole($this->role);

        $auth->assign($role, $this->id);
    }
	
}

































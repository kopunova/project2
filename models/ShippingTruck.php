<?php

namespace app\models;

use Yii;
use app\models\Shipping;
use app\models\Route;
use app\models\City;

/**
 * This is the model class for table "shipping_truck".
 *
 * @property int $id
 * @property int $shipping_id
 * @property int $truck_id
 * @property int $city_start
 * @property int $city_finish
 *
 * @property ShippingDetail[] $shippingDetails
 * @property City $cityFinish
 * @property City $cityStart
 * @property Shipping $shipping
 * @property Truck $truck
 */
class ShippingTruck extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping_truck';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shipping_id', 'truck_id', 'city_start', 'city_finish'], 'integer'],
            [['city_finish'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_finish' => 'id']],
            [['city_start'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_start' => 'id']],
            [['shipping_id'], 'exist', 'skipOnError' => true, 'targetClass' => Shipping::className(), 'targetAttribute' => ['shipping_id' => 'id']],
            [['truck_id'], 'exist', 'skipOnError' => true, 'targetClass' => Truck::className(), 'targetAttribute' => ['truck_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'shipping_id' => 'Shipping ID',
            'truck_id' => 'Truck ID',
            'city_start' => 'City Start',
            'city_finish' => 'City Finish',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingDetails()
    {
        return $this->hasMany(ShippingDetail::className(), ['truck_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityFinish()
    {
        return $this->hasOne(City::className(), ['id' => 'city_finish']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityStart()
    {
        return $this->hasOne(City::className(), ['id' => 'city_start']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping()
    {
        return $this->hasOne(Shipping::className(), ['id' => 'shipping_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTruck()
    {
        return $this->hasOne(Truck::className(), ['id' => 'truck_id']);
    }
	
	public function getTruckName() {
        if (isset($this->truck))
            return $this->truck->name;
        else
            return '';
    }
	
	public function getTruckTonnage() {
        if (isset($this->truck))
            return $this->truck->tonnage;
        else
            return '';
    }
	
	public function getCitiesPresent() {
			
		// получим города присутствия по маршруту
		$shipping = Shipping::findOne($this->shipping_id);
		$route = Route::findOne($shipping->route_id);		
		// города по маршруту
		$cities = $route->citiesObj;
		
		$city_start = City::findOne($this->city_start); 
		$city_finish = City::findOne($this->city_finish); 	
		
		$key_start = array_search($city_start, $cities);
		$key_finish = array_search($city_finish, $cities);
		
		$citiesPresent = [];
		foreach ($cities as $key => $value) {
			
			if ($key >= $key_start && $key <= $key_finish) {
				$citiesPresent[] = $value;				
			}			
		}		
		
		return $citiesPresent;		
		
    }	
	
}














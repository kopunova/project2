<?php

namespace app\models;

use Yii;
use app\models\Order;

/**
 * This is the model class for table "shipping".
 *
 * @property int $id
 * @property string $date_add
 * @property string $date_start
 * @property int $route_id
 * @property string $comment
 *
 * @property Route $route
 * @property ShippingDetail[] $shippingDetails
 */
class Shipping extends \yii\db\ActiveRecord
{	
	
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
			[['date_start', 'route_id'], 'required'],
            [['date_add', 'status'], 'safe'],
            [['route_id'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => Route::className(), 'targetAttribute' => ['route_id' => 'id']],
			[['date_start'], 'filter', 'filter' => function($value) {
                    if ($value == '')
                        return NULL;
                    return date('Y-m-d', strtotime($value));
                }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер',
            'date_add' => 'Дата создания',
			'status' => 'Статус',
			'statusName' => 'Статус',
			'dateAddFormat' => 'Дата создания',
            'date_start' => 'Дата отправления',
			'dateStartFormat' => 'Дата отправления',
            'route_id' => 'Маршрут',
			'routeName' => 'Маршрут',
            'comment' => 'Комментарий',
        ];
    }
	
	public function getStatuses() {

        return [
            '1' => 'Новый',
            '2' => 'В работе',
            '3' => 'Выполнено',
        ];
    }
	
	public function getStatusName() {

        return $this->statuses[$this->status];
    }
	
	public function getDateAddFormat() {
		
		if ($this->date_add) {
			return date('d.m.Y H:i:s', strtotime($this->date_add));
		} else return '';
    }
	
	public function getDateStartFormat() {
		
		if ($this->date_start) {
			return date('d.m.Y', strtotime($this->date_start));
		} else return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Route::className(), ['id' => 'route_id']);
    }
	
	public function getRouteName() {
        if (isset($this->route))
            return $this->route->name;
        else
            return '';
    }
	
	public function getShippingTrucks()
    {
        return $this->hasMany(ShippingTruck::className(), ['shipping_id' => 'id']);
    }
	
	public static function getOrdersInShipping($shipping_id) {

        //$shippingTrucks = $this->shippingTrucks;

        $shippingTrucks = (new \yii\db\Query())->select([
            'id'])
            ->from('shipping_truck')
            ->where(['shipping_id' => $shipping_id])
            ->column();

       /* $ordersInShipping = (new \yii\db\Query())->select([
            'order_id'])
            ->from('shipping_detail')->column();*/

        $orders_id = (new \yii\db\Query())
            ->select('order_id')
            ->distinct()
            ->from('shipping_detail')
            ->where(['IN', 'truck_id', $shippingTrucks])
            ->orderBy('order_id')
            ->column();

        $orders = [];
        foreach ($orders_id as  $order_id) {
            $orders[] = Order::findOne($order_id);
        }

        return $orders;

        //return Order::find()->all();
    }
		
    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getShippingDetails()
    {
        return $this->hasMany(ShippingDetail::className(), ['shipping_id' => 'id']);
    }*/
	
	public function beforeSave($insert) {

        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->date_add = date('Y-m-d H:i:s');
			$this->status = '1';
        }

        return true;
    }
}













<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
use app\models\City;

/**
 * This is the model class for table "route".
 *
 * @property int $id
 * @property string $name
 * @property array $cities
 */
class Route extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'route';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
                ['name', 'required'],
                [['name'], 'string', 'max' => 255],
                [['cities'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'cities' => 'Города',
            'citiesList' => 'Города',
        ];
    }

    public function getCitiesList() {

        if ($this->cities) {

            $query = (new \yii\db\Query())->select([
                        'id', 'name'])
                    ->from('city')
                    ->where(['in', 'id', $this->cities])
                    ->all();

            $cities = array_column($query, 'name', 'id');

            $citiesList = '';
            foreach ($this->cities as $id) {
                $citiesList = $citiesList . (($citiesList == '') ? '' : ', ') . $cities[$id];
            }

            return $citiesList;
        }

        return '';
    }	
	
	public function getCitiesObj() {	
	
		// возвращает список городов по маршруту в виде объектов
		$cities = [];
		foreach ($this->cities as $id) {
			
            $cities[] = City::findOne($id);			
        }		

        return $cities;
    }

    public function afterFind() {
        try {
            if ($this->cities) {
                $this->cities = Json::decode($this->cities);
            }
        } catch (\Exception $e) {
            
        }

        parent::afterFind();
    }

    public function beforeSave($insert) {
				
        if ($this->cities && is_array($this->cities)) {
            // уберем пустые значения
            $this->cities = array_diff($this->cities, ['']);
            $this->cities = array_values($this->cities);
			
			$this->cities = (empty($this->cities)) ? null : Json::encode($this->cities);

        } else {
			 $this->cities = null;			
		}
		
        return parent::beforeSave($insert);
    }
	
	public function checkCities($post) {
		
		if (!isset($post['Route']['cities'])) {
			$this->cities = [];			
		}	

		return true;		
		
	}	

}














<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */

class RbacController extends Controller
{

    public function actionInit()
    {

        $auth = Yii::$app->authManager;

        //var_dump($auth->getRoles());

        $auth->removeAll();

        //return;

        if (!$auth->getRole('admin')) {
            $role = $auth->createRole('admin');
            $role->description = 'Администратор';
            $auth->add($role);

            $auth->assign($role, 1);
        }

        if (!$auth->getRole('logistician')) {
            $role = $auth->createRole('logistician');
            $role->description = 'Логист';
            $auth->add($role);
            //$auth->assign($role, 2);
        }

        if (!$auth->getRole('dispatcher')) {
            $role = $auth->createRole('dispatcher');
            $role->description = 'Диспетчер';
            $auth->add($role);
            //$auth->assign($role, 3);
        }

    }

}


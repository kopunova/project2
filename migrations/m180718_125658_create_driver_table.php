<?php

use yii\db\Migration;

/**
 * Handles the creation of table `driver`.
 */
class m180718_125658_create_driver_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('driver', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'passport' => $this->string(),
            'phone' => $this->string(),
            'truck_number' => $this->string()
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('driver');
    }
}

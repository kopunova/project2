<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shipping`.
 */
class m180622_111301_create_shipping_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('shipping', [
            'id' => $this->primaryKey(),
			'date_add' => $this->datetime(),
			'status' => $this->string(),
			'date_start' => $this->date(),
			'route_id' => $this->integer(),
			'comment' => $this->string()
        ], $tableOptions);
		
		// route_id
		$this->createIndex(
            'idx-shipping-route_id',
            'shipping',
            'route_id'
        );
		
        $this->addForeignKey(
            'fk-shipping-route_id',
            'shipping',
            'route_id',
            'route',
            'id',
            'RESTRICT',
			'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shipping');
    }
}





















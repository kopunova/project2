<?php

use yii\db\Migration;

/**
 * Class m180718_130838_add_column_driver_id_in_order_table
 */
class m180718_130838_add_column_driver_id_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'driver_id', 'integer');

        // driver_id
        $this->createIndex(
            'idx-order-driver_id',
            'order',
            'driver_id'
        );

        $this->addForeignKey(
            'fk-order-driver_id',
            'order',
            'driver_id',
            'driver',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180718_130838_add_column_driver_id_in_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180718_130838_add_column_driver_id_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}

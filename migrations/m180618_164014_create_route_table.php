<?php

use yii\db\Migration;

/**
 * Handles the creation of table `route`.
 */
class m180618_164014_create_route_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('route', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'cities' => $this->text(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('route');
    }
}


























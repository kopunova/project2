<?php

use yii\db\Migration;

/**
 * Class m180711_060843_add_columns_in_order_table
 */
class m180711_060843_add_columns_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'unload_type', 'string');
        $this->addColumn('order', 'cargo_name', 'string');
        $this->addColumn('order', 'cargo_volume', 'integer');
        $this->addColumn('order', 'cargo_quantity_seats', 'integer');
        $this->addColumn('order', 'payment_method', 'string');
        $this->addColumn('order', 'payment_condition', 'string');
        $this->addColumn('order', 'sender', 'string');
        $this->addColumn('order', 'sender_name', 'string');
        $this->addColumn('order', 'sender_phone', 'string');
        $this->addColumn('order', 'receiver', 'string');
        $this->addColumn('order', 'driver', 'string');
        $this->addColumn('order', 'driver_phone', 'string');
        $this->addColumn('order', 'driver_passport', 'string');
        $this->addColumn('order', 'truck_number', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180711_060843_add_columns_in_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180711_060843_add_columns_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}

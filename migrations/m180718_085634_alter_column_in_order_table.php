<?php

use yii\db\Migration;

/**
 * Class m180718_085634_alter_column_in_order_table
 */
class m180718_085634_alter_column_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order', 'cargo_quantity_seats', 'string');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180718_085634_alter_column_in_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180718_085634_alter_column_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `truck`.
 */
class m180622_111847_create_truck_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('truck', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'tonnage' => $this->float(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('truck');
    }
}








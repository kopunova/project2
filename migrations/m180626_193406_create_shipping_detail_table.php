<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shipping_detail`.
 */
class m180626_193406_create_shipping_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('shipping_detail', [
            'id' => $this->primaryKey(),
			'truck_id' => $this->integer(),
			'order_id' => $this->integer(),
			
        ], $tableOptions);
		
		// truck_id
		$this->createIndex(
            'idx-shipping_detail-truck_id',
            'shipping_detail',
            'truck_id'
        );
		
        $this->addForeignKey(
            'fk-shipping_detail-truck_id',
            'shipping_detail',
            'truck_id',
            'shipping_truck',
            'id',
            'CASCADE',
			'CASCADE'
        );	
		
		// order_id
		$this->createIndex(
            'idx-shipping_detail-order_id',
            'shipping_detail',
            'order_id'
        );
		
        $this->addForeignKey(
            'fk-shipping_detail-order_id',
            'shipping_detail',
            'order_id',
            'order',
            'id',
            'RESTRICT',
			'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shipping_detail');
    }
}

















<?php

use yii\db\Migration;

/**
 * Class m180718_090405_add_columns_in_order_table
 */
class m180718_090405_add_columns_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('order', 'client', 'string');
        $this->addColumn('order', 'performer', 'string');
        $this->addColumn('order', 'sender_date', 'date');
        $this->addColumn('order', 'sender_time_from', 'time');
        $this->addColumn('order', 'sender_time_to', 'time');
        $this->addColumn('order', 'receiver_time_from', 'time');
        $this->addColumn('order', 'receiver_time_to', 'time');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180718_090405_add_columns_in_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180718_090405_add_columns_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}

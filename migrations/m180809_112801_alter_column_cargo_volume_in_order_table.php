<?php

use yii\db\Migration;

/**
 * Class m180809_112801_alter_column_cargo_volume_in_order_table
 */
class m180809_112801_alter_column_cargo_volume_in_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->alterColumn('order', 'cargo_volume', 'float');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180809_112801_alter_column_cargo_volume_in_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180809_112801_alter_column_cargo_volume_in_order_table cannot be reverted.\n";

        return false;
    }
    */
}

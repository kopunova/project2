<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `role_column_in_user`.
 */
class m180710_094207_drop_role_column_in_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('user', 'role');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        
    }
}

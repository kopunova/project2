<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m180621_080200_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('order', [
            'id' => $this->primaryKey(),
			'date_add' => $this->datetime(),
			'status' => $this->string(),
			'sum' => $this->float(),
			'weight' => $this->float(),
			'receiver_date' => $this->date(),
			'receiver_city' => $this->integer(),
			'receiver_address' => $this->string(),
			'receiver_name' => $this->string(),
			'receiver_phone' => $this->string(),
			'receiver_email' => $this->string(),
			'sender_city' => $this->integer(),
			'sender_address' => $this->string(),
			'comment' => $this->string()
        ], $tableOptions);
		
		// receiver_city
		$this->createIndex(
            'idx-order-receiver_city',
            'order',
            'receiver_city'
        );
		
        $this->addForeignKey(
            'fk-order-receiver_city',
            'order',
            'receiver_city',
            'city',
            'id',
            'RESTRICT',
			'CASCADE'
        );
		
		// sender_city		
		$this->createIndex(
            'idx-order-sender_city',
            'order',
            'sender_city'
        );
		
        $this->addForeignKey(
            'fk-order-sender_city',
            'order',
            'sender_city',
            'city',
            'id',
            'RESTRICT',
			'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('order');
    }
}




















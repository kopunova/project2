<?php

use yii\db\Migration;

/**
 * Handles the creation of table `shipping_truck`.
 */
class m180625_121226_create_shipping_truck_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
		
        $this->createTable('shipping_truck', [
            'id' => $this->primaryKey(),
			'shipping_id' => $this->integer(),
			'truck_id' => $this->integer(),
			'city_start' => $this->integer(),
			'city_finish' => $this->integer(),
        ], $tableOptions);
		
		// shipping_id
		$this->createIndex(
            'idx-shipping_truck-shipping_id',
            'shipping_truck',
            'shipping_id'
        );
		
        $this->addForeignKey(
            'fk-shipping_truck-shipping_id',
            'shipping_truck',
            'shipping_id',
            'shipping',
            'id',
            'CASCADE',
			'CASCADE'
        );
		
		// truck_id
		$this->createIndex(
            'idx-shipping_truck-truck_id',
            'shipping_truck',
            'truck_id'
        );
		
        $this->addForeignKey(
            'fk-shipping_truck-truck_id',
            'shipping_truck',
            'truck_id',
            'truck',
            'id',
            'RESTRICT',
			'CASCADE'
        );	
		
		// city_start
		$this->createIndex(
            'idx-shipping_truck-city_start',
            'shipping_truck',
            'city_start'
        );
		
        $this->addForeignKey(
            'fk-shipping_truck-city_start',
            'shipping_truck',
            'city_start',
            'city',
            'id',
            'RESTRICT',
			'CASCADE'
        );	
		
		// city_finish
		$this->createIndex(
            'idx-shipping_truck-city_finish',
            'shipping_truck',
            'city_finish'
        );
		
        $this->addForeignKey(
            'fk-shipping_truck-city_finish',
            'shipping_truck',
            'city_finish',
            'city',
            'id',
            'RESTRICT',
			'CASCADE'
        );			
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('shipping_truck');
    }
}

















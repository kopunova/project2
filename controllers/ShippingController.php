<?php

namespace app\controllers;

use Yii;
use app\models\Shipping;
use app\models\ShippingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\Order;
use app\models\Truck;
use app\models\ShippingTruck;
use app\models\ShippingDetail;
use yii\helpers\Json;
use yii\filters\AccessControl;

/**
 * ShippingController implements the CRUD actions for Shipping model.
 */
class ShippingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['logistician'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipping models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShippingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /* Возвращает форму добавления машины в рейс */
    public function actionGetTruckForm() {

        $shipping_id = Yii::$app->request->post('shipping_id');
        $shipping = Shipping::findOne($shipping_id);
		
		// получим список городов по маршруту
        $cities = $shipping->route->citiesObj;
		
		// получим машины		
		$trucks = Truck::find()->orderBy('name')->all();
				
		return $this->renderPartial('truck-form', ['trucks' => $trucks, 'cities' => $cities]);	
		
    }

    /* Добавляет машину к рейсу */
    public function actionAddTruckToShipping() {

		$shipping_id = Yii::$app->request->post('shipping_id');
		$truck_id = Yii::$app->request->post('truck_id');
		$city_start = Yii::$app->request->post('city_start');
		$city_finish = Yii::$app->request->post('city_finish');

		// все поля должны быть заполнены
		if (empty($truck_id) || empty($city_start) || empty($city_finish)) {
			return 'Есть не заполненные поля.';			
		}		
		
		// возможно, такая машина уже существует в рейсе 
		$shippingTruck = ShippingTruck::find()->where(['shipping_id' => $shipping_id])->andWhere(['truck_id' => $truck_id])->one();
		
		if ($shippingTruck) {
			return 'Такая машина уже существует в рейсе.';			
		}
		
		// проверим город старта и город финиша на соответствие маршруту
        $shipping = Shipping::findOne($shipping_id);
        $cities = $shipping->route->cities;

		$key_start = array_search($city_start, $cities);
		$key_finish = array_search($city_finish, $cities);

		if ($key_start === false || $key_finish === false || $key_finish <= $key_start) {
			return 'Не правильно указан город Старта и/или город Финиша.';		
		}

		// параллельных машин быть не должно
        // добавляем машину к рейсу
        $shippingTruckNew = new ShippingTruck();
        $shippingTruckNew->shipping_id = $shipping_id;
        $shippingTruckNew->truck_id = $truck_id;
        $shippingTruckNew->city_start = $city_start;
        $shippingTruckNew->city_finish = $city_finish;

        $citiesPresentNew = $shippingTruckNew->citiesPresent;

        $shippingTrucks = $shipping->shippingTrucks;
        foreach ($shippingTrucks as $shippingTruck) {

            $citiesPresent = $shippingTruck->citiesPresent;

            $count = 0;
            foreach ($citiesPresentNew as $cityPresentNew) {

                if (in_array($cityPresentNew, $citiesPresent) == false) {
                    continue;
                }

                $count = $count + 1;

                if ($count > 1) {
                    return 'Параллельных машин быть не должно.';
                }
            }

        }

        $shippingTruckNew->save();
				
		return true;		
		
    }
	
	//public function actionTest5() {
    public function actionDeleteTruckFromShipping() {
		
		$shipping_truck_id = Yii::$app->request->post('shipping_truck_id');
        $shipping_id = Yii::$app->request->post('shipping_id');

       /* $shipping_truck_id = 5;
        $shipping_id = 7;*/

        $shipping = Shipping::findOne($shipping_id);
        $shippingTrucks = $shipping->shippingTrucks;

        $orders_id = ShippingDetail::find()
            ->select('order_id')
            ->where(['truck_id' => $shipping_truck_id])
            ->column();

        foreach ($shippingTrucks as $shippingTruck) {

            $shippingDetails = $shippingTruck->shippingDetails;

            foreach ($shippingDetails as $shippingDetail) {

                $order_id = $shippingDetail->order_id;

                $key = array_search($order_id, $orders_id);
                if ($key === false) {
                    continue;
                }

                $order = Order::findOne($order_id);
                $order->status = '1';
                $order->save();

                $shippingDetail->delete();

            }
        }
		
		$shippingTruck = ShippingTruck::findOne($shipping_truck_id);
		
		if ($shippingTruck) {

            /*$shippingDetails = $shippingTruck->shippingDetails;

            foreach ($shippingDetails as $shippingDetail) {
                $order = Order::findOne($shippingDetail->order_id);
                $order->status = '1';
                $order->save();
            }*/

			$shippingTruck->delete();
		}

    }
	
	public function actionAddOrdersToShipping() {
		
		$shipping_id = Yii::$app->request->post('shipping_id');
		$orders_id = Yii::$app->request->post('orders_id');
//        $shipping_id = 5;
//        $orders_id = [1];

		$shipping = Shipping::findOne($shipping_id);

        $cities = $shipping->route->citiesObj;

		$shippingTrucks = $shipping->shippingTrucks;

        $shippingTrucksSort = [];
        foreach ($shippingTrucks as $shippingTruck) {
            $city_start = City::findOne($shippingTruck->city_start);
            $key_start = array_search($city_start, $cities);

            $shippingTrucksSort[$shippingTruck->id] = $key_start;
        }

        asort($shippingTrucksSort);

        $shippingTrucksSort2 = [];
        foreach ($shippingTrucksSort as $key => $shippingTruckSort) {
            $shippingTrucksSort2[] = ShippingTruck::findOne($key);
        }

        $shippingTrucks = $shippingTrucksSort2;

        foreach ($orders_id as $order_id) {

            $order = Order::findOne($order_id);

            $city_start = City::findOne($order->sender_city);
            $city_finish = City::findOne($order->receiver_city);

            $key_start = array_search($city_start, $cities);
            $key_finish = array_search($city_finish, $cities);

            foreach ($shippingTrucks as $shippingTruck) {

                $city_start2 = City::findOne($shippingTruck->city_start);
                $city_finish2 = City::findOne($shippingTruck->city_finish);

                $key_start2 = array_search($city_start2, $cities);
                $key_finish2 = array_search($city_finish2, $cities);

                if ($key_start == $key_start2 || ($key_start > $key_start2 && $key_start < $key_finish2)) {

                    $shippingDetail = new ShippingDetail();
                    $shippingDetail->truck_id = $shippingTruck->id;
                    $shippingDetail->order_id = $order_id;

                    $shippingDetail->save();

                    $order = Order::findOne($shippingDetail->order_id);
                    $order->status = $shipping->status;
                    $order->save();

                    if ($key_finish <= $key_finish2) {
                        break;
                    } else {
                        $key_start = $key_finish2;
                    }
                }
            }
        }

    }

    public function actionRemoveOrderFromShipping() {

        $shipping_id = Yii::$app->request->post('shipping_id');
        $order_id = Yii::$app->request->post('order_id');
//        $shipping_id = 5;
//        $order_id = 1;

        $shippingTrucks = (new \yii\db\Query())->select([
            'id'])
            ->from('shipping_truck')
            ->where(['shipping_id' => $shipping_id])
            ->column();

        $shippingDetails = ShippingDetail::find()
            ->where(['IN', 'truck_id', $shippingTrucks])
            ->andWhere(['order_id' => $order_id])
            ->all();

        foreach ($shippingDetails as $shippingDetail) {
            $order = Order::findOne($shippingDetail->order_id);
            $order->status = '1';
            $order->save();

            $shippingDetail->delete();
        }

    }

	public function actionOnChangeRouteInShipping() {	
		
		$shipping_id = Yii::$app->request->post('shipping_id');
		$route_id = Yii::$app->request->post('route_id');	
		
		// запишем новый маршрут в БД 				
		$shipping = Shipping::findOne($shipping_id);
		$shipping->route_id = $route_id;
		$shipping->save();
		
		// удалим все машины, подвязанные к предыдущему маршруту		
		$shippingTrucks = $shipping->shippingTrucks;
		
		foreach ($shippingTrucks as $shippingTruck) {

            $shippingDetails = $shippingTruck->shippingDetails;

            foreach ($shippingDetails as $shippingDetail) {
                $order = Order::findOne($shippingDetail->order_id);
                $order->status = '1';
                $order->save();
            }

			$shippingTruck->delete();
		}
		
    }
	
	public function actionOnChangeStatusInShipping() {	
		
		$shipping_id = Yii::$app->request->post('shipping_id');
		$status = Yii::$app->request->post('status');
		
		// запишем новый статус в БД 				
        $shipping = Shipping::findOne($shipping_id);
		$shipping->status = $status;
		$shipping->save();

        $shippingTrucks = $shipping->shippingTrucks;

        foreach ($shippingTrucks as $shippingTruck) {

            $shippingDetails = $shippingTruck->shippingDetails;

            foreach ($shippingDetails as $shippingDetail) {
                $order = Order::findOne($shippingDetail->order_id);
                $order->status = $status;
                $order->save();
            }
        }

    }
	
	public function actionOnChangeDateStartInShipping() {	
		
		//$shipping_id = Yii::$app->request->post('shipping_id');
		//$status = Yii::$app->request->post('status');	
		
	
		
    }

    public function actionTest() {

        $auth = Yii::$app->authManager;

        //var_dump($auth->getRoles());

        $auth->removeAll();

        //return;

        if (!$auth->getRole('admin')) {
            $role = $auth->createRole('admin');
            $role->description = 'Администратор';
            $auth->add($role);

            $auth->assign($role, 1);
        }

        if (!$auth->getRole('logistician')) {
            $role = $auth->createRole('logistician');
            $role->description = 'Логист';
            $auth->add($role);
            //$auth->assign($role, 2);
        }

        if (!$auth->getRole('dispatcher')) {
            $role = $auth->createRole('dispatcher');
            $role->description = 'Диспетчер';
            $auth->add($role);
            //$auth->assign($role, 3);
        }
    }

	public function actionGetCities() {
		
		$shipping_id = Yii::$app->request->post('shipping_id');
		//$shipping_id = 6;

        $shipping = Shipping::findOne($shipping_id);
		
		// получим города по маршруту
        $cities = $shipping->route->citiesObj;

		// получим машины в рейсе
		$shippingTrucks = $shipping->shippingTrucks;

        $shippingTrucksSort = [];
        foreach ($shippingTrucks as $shippingTruck) {
            $city_start = City::findOne($shippingTruck->city_start);
            $key_start = array_search($city_start, $cities);

            $shippingTrucksSort[$shippingTruck->id] = $key_start;
        }

        asort($shippingTrucksSort);

        $shippingTrucksSort2 = [];
        foreach ($shippingTrucksSort as $key => $shippingTruckSort) {
            $shippingTrucksSort2[] = ShippingTruck::findOne($key);
        }

		$arr_truck = [];
		foreach ($shippingTrucksSort as $key => $shippingTruckSort) {

            $shippingTruck = ShippingTruck::findOne($key);
			
			//$shippingTruck = ShippingTruck::findOne($shippingTruck->id);

            $city_finish2 = City::findOne($shippingTruck->city_finish);
            $key_finish2 = array_search($city_finish2, $cities);

            $shippingDetails = $shippingTruck->shippingDetails;
			
			$arr_orders = [];			
			foreach ($shippingDetails as $shippingDetail) {		
				
				$city_start = City::findOne($shippingDetail->order->sender_city); 
				$city_finish = City::findOne($shippingDetail->order->receiver_city); 
				$weight = $shippingDetail->order->weight;				
				
				$key_start = array_search($city_start, $cities);
				$key_finish = array_search($city_finish, $cities);
				
				$order_city = [];				
				foreach ($cities as $key => $value) {
				    if ($key == $key_finish2) {
                        $order_city[$value->id] = 0;
                        continue;
                    }
					
					if ($key >= $key_start && $key < $key_finish) {
						$order_city[$value->id] = $weight;
					} else {
						$order_city[$value->id] = 0;
					}					

				}
				$arr_orders[$shippingDetail->order_id] = $order_city;			
			}	
			
			$order_city2 = [];			
			foreach ($cities as $city) {
				
				$sum = 0;
				foreach ($arr_orders as $arr_order) {
					foreach ($arr_order as $key => $order) {
						if ($key != $city->id) continue;
						$sum = $sum + $order;						
					}			
				}
				$order_city2[$city->id] = $sum;
			}
						
			$arr_truck[$shippingTruck->id] = $order_city2;
		}

		return $this->renderPartial('cities', ['cities' => $cities, 'shippingTrucks' => $shippingTrucksSort2, 'arr_truck' => $arr_truck]);
    }
	
	public function actionGetOrdersInShipping() {
		
		$shipping_id = Yii::$app->request->post('shipping_id');
		//$shipping_id = 5;
		
		//$shipping = Shipping::findOne($shipping_id);
	
		$ordersInShipping = Shipping::getOrdersInShipping($shipping_id);
				
		return $this->renderPartial('orders-in-shipping', ['ordersInShipping' => $ordersInShipping]);	
		
    }	
	
	public function actionGetOrders($shipping) {

		$orders = Order::getOrdersForShipping($shipping);
				
        return $this->renderPartial('orders', [
            'orders' => $orders,
        ]);
		
    }
	
	public function actionGetContent() {

        $shipping_id = Yii::$app->request->post('shipping_id');
        //$shipping_id = 10;
        $shipping = Shipping::findOne($shipping_id);
		
		$cities = $this->actionGetCities(Yii::$app->request->post());	
		$ordersInShipping = $this->actionGetOrdersInShipping(Yii::$app->request->post());
		$orders = $this->actionGetOrders($shipping);
		
		$content['cities'] = $cities; 
		$content['ordersInShipping'] = $ordersInShipping;
		$content['orders'] = $orders;

        return Json::encode($content);
		
    }

    /**
     * Displays a single Shipping model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shipping model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shipping();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Shipping model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Shipping model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shipping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipping the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shipping::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

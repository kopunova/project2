<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>

<div id="truck-form">

	<div id="truck-form-content">

    <div class="col-md-3">

        <div class="row">
            <div class="col-md-12">

                <label>Добавить машину</label>
                <?= Html::dropDownList('', '', ArrayHelper::Map($trucks, 'id', 'name'), ['id' => 'truck_id', 'prompt' => 'Выберите машину...', 'class' => 'form-group form-control']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= Html::dropDownList('', '', ArrayHelper::Map($cities, 'id', 'name'), ['id' => 'city_start', 'prompt' => 'Выберите город Старта...', 'class' => 'form-group form-control']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= Html::dropDownList('', '', ArrayHelper::Map($cities, 'id', 'name'), ['id' => 'city_finish', 'prompt' => 'Выберите город Финиша...', 'class' => 'form-group form-control']); ?>	
            </div>
        </div>

        <div class="form-group">
            <span class="btn btn-success" id="add-truck-to-shipping-btn">Добавить</span>
            <span class="btn btn-success" id="truck-form-cancel-btn">Отмена</span>
        </div>

    </div>
	
	</div>

</div>































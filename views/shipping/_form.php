<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Route;

/* @var $this yii\web\View */
/* @var $model app\models\Shipping */
/* @var $form yii\widgets\ActiveForm */
$script = <<< JS
    	    
	/* получаем содержимое страницы с сервера */
	getContent();		

    /* Нажата кнопка "Добавить машину к рейсу"
    Получаем форму добавления машины к рейсу */		
	$(document).on("click", "#add-truck-btn", function() {		
		
	    $.ajax({
                type: "POST",
                url: 'get-truck-form',
                data: {
                   shipping_id: {$model->id}, 
                    },
                success: function (data) {
					 $("#truck-form").html($(data).html());	
                }
            });	

		$("#cities-col").removeClass("col-md-12");	
		$("#cities-col").addClass("col-md-9");			
		
    });
	
	/* Добавляем выбранную пользователем машину к рейсу */
	$(document).on("click", "#add-truck-to-shipping-btn", function() {
	    
		$.ajax({
                type: "POST",
                url: 'add-truck-to-shipping',
                data: {
					shipping_id: {$model->id}, 
					truck_id: $('#truck_id').val(), 
					city_start: $('#city_start').val(),
					city_finish: $('#city_finish').val()
				},
                success: function (data) {	
					if (data != true) {
					    alert(data);
						return;
					}					
					getContent();					
					removeTruckForm();					
                }
            });		
						
    });
	
	/* Убирает форму добавления машины к рейсу */
	function removeTruckForm() {
		
		$("#cities-col").removeClass("col-md-9");	
		$("#cities-col").addClass("col-md-12");	
		$('#truck-form-content').remove();			
		
	}	
	
	/* Удаляем машину из рейса */
	$(document).on("click", "#delete-truck-from-shipping", function() {
		
		var shipping_truck_id = parseInt($(this).data('id'));
		
		$.ajax({
                type: "POST",
                url: 'delete-truck-from-shipping',
                data: {
					shipping_truck_id: shipping_truck_id, 
					shipping_id: {$model->id},
				},
                success: function (data) {					
					getContent();
                }
            });	
								
    });	
	
	/* Нажата кнопка "Отмена" в форме добавления машины к рейсу */
	$(document).on("click", "#truck-form-cancel-btn", function() {		
		removeTruckForm();
    });	
	
    /* Получает содержимое страницы с сервера */
	function getContent() {	    	
	    
		$.ajax({
                type: "POST",
				dataType: 'json',
                url: 'get-content',
                data: {
					shipping_id: {$model->id},
					},
                success: function (data) {															
					$("#cities").html($(data['cities']).html());
					$("#orders-in-shipping").html($(data['ordersInShipping']).html());
					$("#orders").html($(data['orders']).html());
                }
            });       
    }   
    
    /* Переносим выбранные пользователем Заявки в рейс */    
	$(document).on("click", "#add-orders-to-shipping-btn", function() {
	    		    
		var trs = $('#orders-tbl').find("tr");
		
		var orders_id = [];			
		for (i = 0; i < trs.length; i++) {
			if ($(trs[i]).data('check') == 1) {
				orders_id.push($(trs[i]).data('id')); 				
			}
		}
		
		if (orders_id.length == 0) {
			alert('Отметьте флажками необходимые заявки.');
			return;	
		}		
					
		$.ajax({
                type: "POST",
                url: 'add-orders-to-shipping',
                data: {
					shipping_id: {$model->id},
					orders_id: orders_id
				},
                success: function (data) {
					getContent();					
                }
            });			
		
    });
	
	/* Удаляет Заявку из рейса */	
	$(document).on("click", "#remove-order-from-shipping", function() {
	    
		var order_id = $(this).data('order_id');	
					
		$.ajax({
                type: "POST",
                url: 'remove-order-from-shipping',
                data: {
                    shipping_id: {$model->id},
				    order_id: order_id
				},
                success: function (data) {
					getContent();					
                }
            });					
    });	
	
	/* Отмечает/снимает отметку Заявки в общем списке Заявок */
	$(document).on("click", "#order-tr", function() {
				
		var i = $(this).find("i");

		if ($(this).data('check') == 0) {	
			$(i).removeClass('fa fa-square-o');
			$(i).addClass('fa fa-check-square-o');				
			$(this).data('check', 1);
		} else {
			$(i).removeClass('fa fa-check-square-o');
			$(i).addClass('fa fa-square-o');
			$(this).data('check', 0);
		}	
		
    });	
	
	//var route_id = $('#route_id').val(); 
	
	/* При изменении маршрута в рейсе */
	$(document).on("change", "#route_id", function() {
	    
		/*var dialog = confirm('Будут удалены все машины из текущего рейса. Продолжить? ');

        if (dialog == false) { 
            //$('#route_id').val(route_id);             
			return;	
		}*/
		
		//route_id = $('#route_id').val();	
		
		$.ajax({
                type: "POST",
                url: 'on-change-route-in-shipping',
                data: {
					shipping_id: {$model->id},
					route_id: $('#route_id').val()						
				},
                success: function (data) {	
					getContent();
                }
        });	

    });	
	
	/* При изменении даты отправления */
	$(document).on("change", "#date_start", function() {

		//alert($('#date_start').val());	
				
		/*$.ajax({
                type: "POST",
                url: 'on-change-status-in-shipping',
                data: {
					shipping_id: {$model->id},
					date_start: $('#date_start').val()						
				},
                success: function (data) {	
					getContent();
                }
        });*/	

    });	
	
	/*function test(callback) {
      $.ajax({
            type: "POST",
            url: "/order/get-load",
            data: $(this).closest('form').serialize(),
            success: function(msg){
                $('#load').html($(msg).html());
                callback();
            }
        });
  }
  
  test(function(){
       // code
  });*/
	
	/* При изменении статуса рейса */	
	$(document).on("change", "#status", function() {
				
		$.ajax({
                type: "POST",
                url: 'on-change-status-in-shipping',
                data: {
					shipping_id: {$model->id},
					status: $('#status').val()						
				},
                success: function (data) {	
					getContent();
                }
        });	

    });		

JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>

<div class="shipping-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <div class="row">
            <div class="col-md-4">

                <?php $routes = Route::find()->orderBy('name')->all(); ?>
                <?=
                $form->field($model, 'route_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($routes, 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите маршрут...', 'id' => 'route_id'],
                    'pluginOptions' => [
                        'allowClear' => false
                    ],
                ]);
                ?>

            </div>

            <div class="col-md-2">
                <?=
                $form->field($model, 'date_start')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control', 'id' => 'date_start', 'disabled' => true],
                ])
                ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'dateAddFormat')->textInput(['readonly' => 'readonly']) ?>	
            </div>
            <div class="col-md-4">	
                <?= $form->field($model, 'status')->dropDownList($model->statuses, ['id' => 'status']); ?>				
            </div>

        </div>	

        <div class="row">
            <div id="cities-col" class="col-md-12">
                <div id="cities">

                </div>				
            </div>
            <div id="truck-form">

            </div>			
        </div>
		
		<div id="orders-in-shipping">					

        </div>

        <div id="orders">				

        </div>	

    </div>

    <?php ActiveForm::end(); ?>

</div>


















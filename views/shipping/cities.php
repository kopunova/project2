
<div id="cities">

    <div class="table-responsive">

        <table class="table table-bordered">
            <tr>
                <th>Город</th>
                <?php foreach ($shippingTrucks as $shippingTruck): ?>
                    <th colspan="2"><?= $shippingTruck['truckName'] . ' (' . $shippingTruck['truckTonnage'] . ' т)' ?> <i class="fa fa-remove" id="delete-truck-from-shipping" data-id=<?= $shippingTruck['id'] ?> style="font-size:15px; color:green;" title="Удалить машину из рейса"></i></th>
                <?php endforeach; ?>	
                <th style="width: 50px"><span class="btn btn-success" id="add-truck-btn" title="Добавить машину к рейсу"><i class="fa fa-bus"></i> <i class="fa fa-plus"></i></span></th>				
            </tr>

            <?php foreach ($cities as $city): ?>		
                <tr>
                    <td><?= $city->name ?></td>   
                    <?php foreach ($shippingTrucks as $shippingTruck): ?>
                        <td>
                            <?php if (in_array($city, $shippingTruck->citiesPresent)): ?>					
                                <i class="fa fa-bus" style="color: green;"></i>
                            <?php endif; ?>	
                        </td>
                        <td>
                            <?php if (in_array($city, $shippingTruck->citiesPresent)): ?>					
                                <?= $arr_truck[$shippingTruck->id][$city->id] ?>
                            <?php endif; ?>								
                        </td>
                    <?php endforeach; ?>	
                    <td></td>					
                </tr>	
            <?php endforeach; ?>	

        </table>	

    </div>

</div>














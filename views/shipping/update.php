<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Shipping */

$this->title = 'Рейс №' . $model->id . ' от ' . $model->dateAddFormat;
$this->params['breadcrumbs'][] = ['label' => 'Рейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Рейс №' . $model->id . ' от ' . $model->dateAddFormat, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="shipping-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

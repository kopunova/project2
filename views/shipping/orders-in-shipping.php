
<div id="orders-in-shipping">

<label>Заявки в рейсе</label>
      
                    <table class="table table-bordered">
                        <tr>			
                            <th>№</th>			
                            <th>Номер</th>
                            <th>Дата создания</th>
                            <th>Дата получения</th>		
                            <th>Город отправления</th>
                            <th>Город получения</th>
                            <th>Вес, т</th>   
                            <th>Статус</th>		
                            <th></th>			
                        </tr>

                        <?php foreach ($ordersInShipping as $key => $order): ?>
                            <tr>				 
                                <td><?= $key + 1 ?></td>
                                <td><?= $order->id ?></td>   
                                <td><?= $order->dateAddFormat ?></td>  
                                <td><?= $order->receiverDateFormat ?></td> 				
                                <td><?= $order->senderCityName ?></td>   
                                <td><?= $order->receiverCityName ?></td>   
                                <td><?= $order->weight ?></td>  
                                <td><?= $order->statusName ?></td> 
                                <td><i class="fa fa-remove" id="remove-order-from-shipping" data-order_id=<?= $order->id ?> style="font-size:18px; color:green;" title="Удалить заявку из рейса"></i></td>				
                            </tr>
                        <?php endforeach; ?>	

                    </table>	                      

</div>































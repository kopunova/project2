<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shipping */

$this->title = 'Создание нового рейса';
$this->params['breadcrumbs'][] = ['label' => 'Рейсы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-create', [
        'model' => $model,
    ]) ?>

</div>

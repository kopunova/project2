<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShippingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Рейсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
	<div class="panel panel-default panel-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                    'attribute' => 'date_add',
                    'content' => function($data) {
                        return $data->dateAddFormat;
                    }
                ],
			[
                    'attribute' => 'date_start',
                    'content' => function($data) {
                        return $data->dateStartFormat;
                    }
                ],
			[
                    'attribute' => 'route_id',
                    'content' => function($data) {
                        return $data->routeName;
                    }
                ],
			[
                    'attribute' => 'status',
                    'content' => function($data) {
                        return $data->statusName;
                    }
                ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	
	</div>
</div>










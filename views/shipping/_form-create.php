<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Route;

/* @var $this yii\web\View */
/* @var $model app\models\Shipping */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shipping-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="panel panel-default panel-body">
	
			<?=
					$form->field($model, 'date_start')->widget(\yii\jui\DatePicker::classname(), [
						'language' => 'ru',
						'dateFormat' => 'dd.MM.yyyy',
						'options' => ['class' => 'form-control'],
					])
					?>
				
	
		<?php $routes = Route::find()->orderBy('name')->all(); ?>
						
		<?=
                $form->field($model, 'route_id')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($routes, 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите маршрут...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>	
				
	</div>

    <div class="form-group">
        <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


















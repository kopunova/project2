
<div id="orders">

    <div class="form-group">
        <span class="btn btn-success" id="add-orders-to-shipping-btn">Перенести в рейс</span>
    </div>

    <table class="table table-bordered" id="orders-tbl">
        <tr>
            <th>№</th>
            <th></th>
            <th>Номер</th>
            <th>Дата создания</th>
            <th>Дата получения</th>
            <th>Город отправления</th>
            <th>Город получения</th>
            <th>Вес, т</th>
            <th>Статус</th>
        </tr>

        <?php foreach ($orders as $key => $order): ?>
            <tr id="order-tr" data-check="0" data-id=<?= $order->id ?>>
                <td><?= $key + 1 ?></td>
                <td width="40px"><i class="fa fa-square-o" style="font-size:18px;"></i></td>
                <td><?= $order->id ?></td>
                <td><?= $order->dateAddFormat ?></td>
                <td><?= $order->receiverDateFormat ?></td>
                <td><?= $order->senderCityName ?></td>
                <td><?= $order->receiverCityName ?></td>
                <td><?= $order->weight ?></td>
                <td><?= $order->statusName ?></td>
            </tr>
        <?php endforeach; ?>

    </table>

</div>



















<aside class="main-sidebar">

    <section class="sidebar">

        <?php
            $items = [];
            $items[] = ['label' => 'Главное меню', 'options' => ['class' => 'header']];
            $items[] = ['label' => 'Заявки', 'icon' => 'book', 'url' => ['order/index']];
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('logistician')) {
                $items[] = ['label' => 'Рейсы', 'icon' => 'archive', 'url' => ['shipping/index']];
            }
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('logistician')) {
                $items[] = ['label' => 'Маршруты', 'icon' => 'exchange', 'url' => ['route/index']];
            }
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('logistician')) {
                $items[] = ['label' => 'Машины', 'icon' => 'bus', 'url' => ['truck/index']];
            }
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('logistician')) {
                $items[] = ['label' => 'Водители', 'icon' => 'child', 'url' => ['driver/index']];
            }
            if (Yii::$app->user->can('admin') || Yii::$app->user->can('logistician')) {
            $items[] = ['label' => 'Города', 'icon' => 'building-o', 'url' => ['city/index']];
            }
            if (Yii::$app->user->can('admin')) {
                $items[] = ['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['user/index']];
            }
        ?>

        <?= dmstr\widgets\Menu::widget( 
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $items,
            ]
        ) ?>

    </section>

</aside>

<!--['label' => 'Главное меню', 'options' => ['class' => 'header']],
['label' => 'Заявки', 'icon' => 'book', 'url' => ['order/index']],
['label' => 'Рейсы', 'icon' => 'archive', 'url' => ['shipping/index']],
['label' => 'Маршруты', 'icon' => 'exchange', 'url' => ['route/index']],
['label' => 'Машины', 'icon' => 'bus', 'url' => ['truck/index']],
['label' => 'Города', 'icon' => 'building-o', 'url' => ['city/index']],
['label' => 'Пользователи', 'icon' => 'user-o', 'url' => ['user/index']],  -->

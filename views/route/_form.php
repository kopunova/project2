<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\City;

/* @var $this yii\web\View */
/* @var $model app\models\Route */
/* @var $form yii\widgets\ActiveForm */
$script = <<< JS

	$(document).on("click", "#addCity", function(e){
      e.preventDefault();
	  var _this = $(this);
      var i = parseInt(_this.data('id')) + 1;
	  _this.data('id', i);
	  
      var tmpl = $.templates("#selectTemplate");
      var data = {id: i};     
      var html = tmpl.render(data);
      $('#cities').append(html);
    });
	
	$(document).on("click", "#removeCity", function(e){
		$(this).parent().parent().remove();		
    });
    
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>

<div class="route-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <?= $form->field($model, 'name')->textInput() ?>

        <?php $cities = City::find()->orderBy('name')->all(); ?>

        <?= Html::label($model->attributeLabels()['cities']); ?>

        <div id="cities">

            <?php if ($model->cities && is_array($model->cities)): ?>

                <?php foreach ($model->cities as $key => $value): ?>
                    <div class="row">	
                        <div class="col-md-11"> 

                            <?=
                            $form->field($model, 'cities['.$key.']')->dropDownList(ArrayHelper::Map($cities, 'id', 'name'), ['prompt' => 'Выберите значение...'])->label(false);
                            ?>
                        </div>
                        <div class="col-md-1">
                            <span class="btn btn-success" id="removeCity"><i class="fa fa-remove"></i></span> 
                        </div>

                    </div>				

                <?php endforeach; ?>

            <?php endif; ?>

        </div>

        <span class="btn btn-success" id="addCity" data-id="<?= count($model->cities) - 1 ?>">Добавить город</span>

    </div>

    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script id="selectTemplate" type="text/html">
    <div class="row">	
        <div class="col-md-11"> 

            <div class="form-group field-route-{{:id}}-cities">

                <select id="route-{{:id}}-cities" class="form-control" name="Route[cities][{{:id}}]">
                    <option value="">Выберите значение...</option>
                    <?php foreach ($cities as $city): ?>
                        <option value="<?= $city->id ?>"><?= $city->name ?></option>	
                    <?php endforeach; ?>
                </select>

                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-1">
            <span class="btn btn-success" id="removeCity"><i class="fa fa-remove"></i></span>
        </div>
    </div>
</script>
















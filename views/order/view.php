<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Заявка №' . $model->id . ' от ' . $model->dateAddFormat;
$this->params['breadcrumbs'][] = ['label' => 'Заявки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'dateAddFormat',
            'senderCityName',
            'sender_address',
            'sender',
            'sender_name',
            'sender_phone',
            'senderDateFormat',
            'sender_time_from',
            'sender_time_to',
            'receiverCityName',
            'receiver_address',
			'receiver',
            'receiver_name',
            'receiver_phone',
            'receiver_email',
            'receiverDateFormat',
            'receiver_time_from',
            'receiver_time_to',
            'client',
            'performer',
            'unloadTypeName',
            'cargo_name',
            'weight',
            'cargo_volume',
            'cargo_quantity_seats',
            'sum',
            'paymentMethodName',
            'payment_condition',
            'driverName',
            'comment',
            'statusName',
        ],
    ])
    ?>

</div>








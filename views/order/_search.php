<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date_add') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'sum') ?>

    <?= $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'receiver_date') ?>

    <?php // echo $form->field($model, 'receiver_city') ?>

    <?php // echo $form->field($model, 'receiver_address') ?>

    <?php // echo $form->field($model, 'receiver_name') ?>

    <?php // echo $form->field($model, 'receiver_phone') ?>

    <?php // echo $form->field($model, 'receiver_email') ?>

    <?php // echo $form->field($model, 'sender_city') ?>

    <?php // echo $form->field($model, 'sender_address') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

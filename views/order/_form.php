<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\select2\Select2;
use app\models\City;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */

$formatJs = <<< 'JS'

var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' + 
    '<div class="col-sm-12">' +
        '<div><b style="margin-left:5px">' + repo.name + '</b></div>' + 
        '<div><b style="margin-left:5px">' + repo.phone + '</b></div>' + 
        '<div><b style="margin-left:5px">' + repo.passport + '</b></div>' + 
        '<div><b style="margin-left:5px">' + repo.truck_number + '</b></div>' + 
    '</div>' +
'</div>';   
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.name || repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-default panel-body">

        <?php $cities = City::find()->orderBy('name')->all(); ?>

        <div class="row">
            <div class="col-md-3">
                <?=
                $form->field($model, 'sender_city')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($cities, 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите город...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>		
            </div>
            <div class="col-md-9">
                <?= $form->field($model, 'sender_address')->textInput(['maxlength' => true, 'placeholder' => 'ул. Каштановая, 17']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'sender')->textInput(['maxlength' => true, 'placeholder' => 'ООО "ВИД"']) ?>
            </div>
            <div class="col-md-7">
                <?= $form->field($model, 'sender_name')->textInput(['maxlength' => true, 'placeholder' => 'Игорь Васильевич, логист']) ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'sender_phone')->textInput(['maxlength' => true]) ?>

            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?=
                $form->field($model, 'sender_date')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ])
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'sender_time_from')->widget(\kartik\time\TimePicker::classname(), [
                    'pluginOptions' => [
                        'showMeridian' => false,
                        'format' => "hh:mm",
                        'defaultTime' => false,
                        'minuteStep' => 5,
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'sender_time_to')->widget(\kartik\time\TimePicker::classname(), [
                    'pluginOptions' => [
                        'showMeridian' => false,
                        'format' => "hh:mm",
                        'defaultTime' => false,
                        'minuteStep' => 5,
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="panel panel-default panel-body">

        <div class="row">
            <div class="col-md-3"> 	
                <?=
                $form->field($model, 'receiver_city')->widget(Select2::classname(), [
                    'data' => ArrayHelper::Map($cities, 'id', 'name'),
                    'options' => ['placeholder' => 'Выберите город...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>				
            </div>
            <div class="col-md-9">
                <?= $form->field($model, 'receiver_address')->textInput(['maxlength' => true, 'placeholder' => 'ул. Ленина, 34 "Б"']) ?>
            </div>          

        </div>

        <div class="row">

            <div class="col-md-3">
                <?= $form->field($model, 'receiver')->textInput(['maxlength' => true, 'placeholder' => 'ООО "Синие тучи"']) ?>
            </div>

            <div class="col-md-7">
                <?= $form->field($model, 'receiver_name')->textInput(['maxlength' => true, 'placeholder' => 'Ренат Носов, менеджер']) ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'receiver_phone')->textInput(['maxlength' => true]) ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-3">
                <?=
                $form->field($model, 'receiver_date')->widget(\yii\jui\DatePicker::classname(), [
                    'language' => 'ru',
                    'dateFormat' => 'dd.MM.yyyy',
                    'options' => ['class' => 'form-control'],
                ])
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'receiver_time_from')->widget(\kartik\time\TimePicker::classname(), [
                    'pluginOptions' => [
                        'showMeridian' => false,
                        'format' => "hh:mm",
                        'defaultTime' => false,
                        'minuteStep' => 5,
                    ],
                ]); ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'receiver_time_to')->widget(\kartik\time\TimePicker::classname(), [
                    'pluginOptions' => [
                        'showMeridian' => false,
                        'format' => "hh:mm",
                        'defaultTime' => false,
                        'minuteStep' => 5,
                    ],
                ]); ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default panel-body">

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'client')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'performer')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'unload_type')->dropDownList($model->unloadTypes, ['prompt' => 'Выберите значение...']); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cargo_name')->textInput(['maxlength' => true, 'placeholder' => 'Пищевые добавки']) ?>
            </div>
            <div class="col-md-1">
                <?= $form->field($model, 'weight')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'cargo_volume')->textInput() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'cargo_quantity_seats')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'sum')->textInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'payment_method')->dropDownList($model->paymentMethods, ['prompt' => 'Выберите значение...']); ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'payment_condition')->textInput(['maxlength' => true, 'placeholder' => 'После доставки груза']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'driver_id')->widget(Select2::classname(), [
                    'name' => 'kv-repo-template',
                    'value' => '14719648',
                    'initValueText' => $model->driverName,
                    'options' => [
                        'placeholder' => 'Поиск по полям "Имя", "Телефон", "Марка и гос.№ трансп. средства"',
                        'id' => 'driver_id',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['order/search-driver']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('formatRepo'),
                        'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $this->render('_driver_info', [
                    'model' => $model->driverInst,
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'comment')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'status')->dropDownList($model->statuses); ?>	 
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'dateAddFormat')->textInput(['disabled' => true]) ?>
            </div>
        </div>    
    </div>

    <div class="form-group">
        <?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>










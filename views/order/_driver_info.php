<?php

$script = <<< JS
    $(document).ready(function(){
        $(document).on("change", "#driver_id", function(e){
            var _this = $(this);
            $.ajax({
                type: "GET",
                url: "/order/get-driver-info",
                data:{id:_this.val()},
                success: function(msg){
                    $("#driver_info").html($(msg));
                },               
            });
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

?>

<div id="driver_info">
    <?php if ($model): ?>

        <?= $model->phone ?> <br>
        <?= $model->passport ?> <br>
        <?= $model->truck_number ?> <br><br>

    <?php endif ?>

</div>
























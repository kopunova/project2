<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="panel panel-default panel-body">	

        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                'id',
                    [
                    'attribute' => 'date_add',
                    'content' => function($data) {
                        return $data->dateAddFormat;
                    }
                ],
                    [
                    'attribute' => 'status',
                    'content' => function($data) {
                        return $data->statusName;
                    }
                ],
                    [
                    'attribute' => 'sender_city',
                    'content' => function($data) {
                        return $data->senderCityName;
                    }
                    ],
                [
                    'attribute' => 'receiver_city',
                    'content' => function ($data) {
                        return $data->receiverCityName;
                    }
                ],
                [
                    'attribute' => 'receiver_date',
                    'content' => function ($data) {
                        return $data->receiverDateFormat;
                    }
                ],
                'weight',
                'sum',
                //'receiver_date',
                //'receiver_city',
                //'receiver_address',
                //'receiver_name',
                //'receiver_phone',
                //'receiver_email:email',
                //'sender_city',
                //'sender_address',
                //'comment',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>

    </div>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Truck */

$this->title = 'Создание новой машины';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="truck-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Truck */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="truck-form">

    <?php $form = ActiveForm::begin(); ?>

	<div class="panel panel-default panel-body">

		<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

		<?= $form->field($model, 'tonnage')->textInput() ?>

	</div>

	<div class="form-group">
		<?= Html::submitButton('Записать', ['class' => 'btn btn-success']) ?>
	</div>	

    <?php ActiveForm::end(); ?>

</div>
